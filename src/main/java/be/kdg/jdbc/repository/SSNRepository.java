package be.kdg.jdbc.repository;

import be.kdg.jdbc.model.SSN;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SSNRepository extends JpaRepository<SSN, Long> {
    Optional<SSN> findBySsnNumber(String ssnNumber);
}
