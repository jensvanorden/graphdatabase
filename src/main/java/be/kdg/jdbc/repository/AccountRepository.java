package be.kdg.jdbc.repository;

import be.kdg.jdbc.model.AccountHolder;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;


@Repository
public interface AccountRepository extends JpaRepository<AccountHolder, Long> {
}
