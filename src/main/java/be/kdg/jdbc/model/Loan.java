package be.kdg.jdbc.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;

@Entity
@Table(name = "Loans")
@Getter
@Setter
public class Loan {
    @javax.persistence.Id
    @GeneratedValue
    private Long loanId;
    private Double amount;
    private Double balance;
}
