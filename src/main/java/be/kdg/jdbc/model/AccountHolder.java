package be.kdg.jdbc.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "AccountHolders")
@Getter
@Setter
public class AccountHolder {
    @Id
    @GeneratedValue
    private Long holderId;
    private String firstName;
    private String lastName;

    @ManyToOne
    private SSN ssn;
    @ManyToOne
    private Address address;
    @ManyToOne
    private PhoneNumber phoneNumber;
    @OneToMany
    @JoinColumn(name = "HolderId")
    private List<BankAccount> bankAccounts = new ArrayList<>();
    @OneToMany
    @JoinColumn(name = "HolderId")
    private List<CreditCard> creditCards = new ArrayList<>();
    @OneToMany
    @JoinColumn(name = "HolderId")
    private List<Loan> loans = new ArrayList<>();
}
