package be.kdg.jdbc.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;

@Entity
@Table(name = "Adresses")
@Getter
@Setter
public class Address {
    @javax.persistence.Id
    @GeneratedValue
    private Long addressId;
    private String street;
    private String city;
    private Integer zipCode;
}
