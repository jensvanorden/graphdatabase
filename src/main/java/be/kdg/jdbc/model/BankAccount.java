package be.kdg.jdbc.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;

@Entity
@Table(name = "BankAccounts")
@Getter
@Setter
public class BankAccount {
    @javax.persistence.Id
    @GeneratedValue
    private Long bankAccountId;
    private String cardNumber;
    private Double balance;
}
