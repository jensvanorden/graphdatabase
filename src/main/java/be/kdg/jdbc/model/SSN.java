package be.kdg.jdbc.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;

@Entity
@Table(name = "SocialSecurityNumbers")
@Getter
@Setter
public class SSN {
    @javax.persistence.Id
    @GeneratedValue
    private Long ssnId;
    private String ssnNumber;
}
