package be.kdg.jdbc.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "CreditCards")
@Getter
@Setter
public class CreditCard {
    @javax.persistence.Id
    @GeneratedValue
    private Long creditCardId;
    private String cardNumber;
    private Integer creditLimit;
    private Double balance;
    private String expirationDate;
    private Integer securityCode;
}
