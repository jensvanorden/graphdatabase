package be.kdg.jdbc.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;

@Entity
@Table(name = "PhoneNumbers")
@Getter
@Setter
public class PhoneNumber {
    @javax.persistence.Id
    @GeneratedValue
    private Long phoneNumberId;
    private String phoneNumber;
}
