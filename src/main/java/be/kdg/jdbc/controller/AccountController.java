package be.kdg.jdbc.controller;

import be.kdg.jdbc.model.AccountHolder;
import be.kdg.jdbc.service.AccountService;
import be.kdg.jdbc.model.dto.AccountHolderDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/accounts")
public class AccountController {
    private AccountService accountService;
    private ObjectMapper mapper = new ObjectMapper();

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping("/post")
    public ResponseEntity<AccountHolder> postAccountHolder(@RequestBody String dtoString) {
        System.out.println(dtoString);
        try {
            AccountHolderDto dto = mapper.readValue(dtoString, AccountHolderDto.class);
            AccountHolder holder = accountService.createAccountHolder(dto);
            return ResponseEntity.ok(holder);
        } catch (Exception e) {
            System.out.println(e);
        }
        return ResponseEntity.ok(null);
    }
}
