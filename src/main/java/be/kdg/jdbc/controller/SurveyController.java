package be.kdg.jdbc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SurveyController {
    @GetMapping(value = "/")
    public ModelAndView index(ModelAndView mav) {
        mav.setViewName("index");
        return mav;
    }

    @GetMapping(value = "/test")
    public ModelAndView full(ModelAndView mav) {
        mav.setViewName("full_form");
        return mav;
    }
}
