package be.kdg.jdbc.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = "be.kdg.jdbc.repository")
public class PersistenceConfig {
}
