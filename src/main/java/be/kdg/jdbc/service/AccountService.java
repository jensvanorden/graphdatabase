package be.kdg.jdbc.service;

import be.kdg.jdbc.model.*;
import be.kdg.jdbc.repository.*;
import be.kdg.jdbc.model.dto.AccountHolderDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AccountService {
    private AccountRepository accountRepository;
    private AddressRepository addressRepository;
    private PhoneRepository phoneRepository;
    private SSNRepository ssnRepository;
    private BankAccountRepository bankAccountRepository;
    private CreditCardRepository creditCardRepository;
    private LoanRepository loanRepository;

    @Autowired
    public AccountService(AccountRepository accountRepository, AddressRepository addressRepository, PhoneRepository phoneRepository, SSNRepository ssnRepository, BankAccountRepository bankAccountRepository, CreditCardRepository creditCardRepository, LoanRepository loanRepository) {
        this.accountRepository = accountRepository;
        this.addressRepository = addressRepository;
        this.phoneRepository = phoneRepository;
        this.ssnRepository = ssnRepository;
        this.bankAccountRepository = bankAccountRepository;
        this.creditCardRepository = creditCardRepository;
        this.loanRepository = loanRepository;
    }

    private Generator generator = new Generator();

    public AccountHolder createAccountHolder(AccountHolderDto dto) {
        Optional<Address> optionalAddress = addressRepository.findByStreetAndCityAndZipCode(dto.getStreet(), dto.getCity(), dto.getZipCode());
        Address address;
        if (optionalAddress.isPresent()) {
            address = optionalAddress.get();
        }
        else {
            address = new Address();
            address.setStreet(dto.getStreet());
            address.setCity(dto.getCity());
            address.setZipCode(dto.getZipCode());
            addressRepository.save(address);
        }

        Optional<PhoneNumber> optionalPhoneNumber = phoneRepository.findByPhoneNumber(dto.getPhoneNumber());
        PhoneNumber phoneNumber;
        if (optionalPhoneNumber.isPresent()) {
            phoneNumber = optionalPhoneNumber.get();
        }
        else {
            phoneNumber = new PhoneNumber();
            phoneNumber.setPhoneNumber(dto.getPhoneNumber());
            phoneRepository.save(phoneNumber);
        }

        Optional<SSN> optionalSSN = ssnRepository.findBySsnNumber(dto.getSsnNumber());
        SSN ssn;
        if (optionalSSN.isPresent()) {
            ssn = optionalSSN.get();
        }
        else {
            ssn = new SSN();
            ssn.setSsnNumber(dto.getSsnNumber());
            ssnRepository.save(ssn);
        }

        AccountHolder holder = new AccountHolder();
        holder.setFirstName(dto.getFirstName());
        holder.setLastName(dto.getLastName());
        holder.setAddress(address);
        holder.setPhoneNumber(phoneNumber);
        holder.setSsn(ssn);

        if (dto.isBankAccount()) {
            BankAccount bankAccount = new BankAccount();
            bankAccount.setCardNumber(generator.getCardNumber("BE"));
            bankAccount.setBalance(generator.getBalance());
            bankAccountRepository.save(bankAccount);
            holder.getBankAccounts().add(bankAccount);
        }

        if (dto.isCreditCard()) {
            CreditCard creditCard = new CreditCard();
            creditCard.setCardNumber(generator.getCardNumber(""));
            creditCard.setCreditLimit(generator.getCreditLimit());
            creditCard.setBalance(0.0);
            creditCard.setExpirationDate(generator.getExpirationDate());
            creditCard.setSecurityCode(generator.getSecurityCode());
            creditCardRepository.save(creditCard);
            holder.getCreditCards().add(creditCard);
        }

        if (dto.isLoan()) {
            Loan loan = new Loan();
            loan.setAmount(generator.getLoanAmount());
            loan.setBalance(0.0);
            loanRepository.save(loan);
            holder.getLoans().add(loan);
        }

        accountRepository.save(holder);
        return holder;
    }

}
